
angular.module('flapperNews', [])
.factory('posts', [function(){
  factory = {};

  factory.list = [
    {title: 'baidu', link: 'www.baidu.com', upvotes: 5}
  ];

  factory.addItem = function(item) {
    factory.list.push(item);
  };
  

  factory.deleteItem = function(item) {
    index = factory.list.indexOf(item);
    factory.list.splice(index, 1);
  };
  
  
  return factory;
}])
.controller('MainCtrl', [
'$scope',
'posts',
function($scope,posts){
  $scope.test = 'Hello world!';

  $scope.posts = posts.list;

  $scope.addPost = function(){

    posts.addItem({
      title: $scope.title,
      link: $scope.link,
      upvotes: 0
    });  	
    
  };



  $scope.deleteItem = function(post) {
    posts.deleteItem(post);
  };

  $scope.incrementUpvotes = function(post) {
    post.upvotes += 1;
  };

}]);
